package pl.telemedis.services.integraFHIR.server.config;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import pl.telemedis.services.integraFHIR.server.interceptor.OAuth2Interceptor;

public class FhirApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(OAuth2Interceptor.class);

	public static final int PORT = 8082;

	public static void main(String[] args) throws Exception {
		int port = PORT;
		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("--port")) {
				port = Integer.valueOf(args[i].split("=")[1]);
				break;
			}
		}

		LOGGER.info("main: port={}", port);
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		LOGGER.info("main: context={}", context);

		context.setContextPath("/");
		Server jettyServer = new Server(port);
		LOGGER.info("main: ettyServer={}", jettyServer);

		jettyServer.setHandler(context);

		context.addEventListener(new ContextLoaderListener());
		context.setInitParameter("contextClass", AnnotationConfigWebApplicationContext.class.getName());
		context.setInitParameter("contextConfigLocation", FhirConfiguration.class.getName());

		ServletHolder jerseyServlet = context.addServlet(FhirServer.class, "/*");
		LOGGER.info("main: jerseyServlet={}", jerseyServlet);

		jerseyServlet.setInitOrder(1);
		jerseyServlet.setInitParameter("ImplementationDescription", "FHIR JPA Server");
		jerseyServlet.setInitParameter("FhirVersion", "DSTU3");
		String test = null;
		LOGGER.info("main: test={}", test);
		jettyServer.start();
		LOGGER.info("main: port={}", test);
	}

}
